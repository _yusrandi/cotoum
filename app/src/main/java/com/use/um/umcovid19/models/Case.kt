package com.use.um.umcovid19.models

import com.google.gson.annotations.SerializedName

data class Case(
    @SerializedName("kec_id") var name : String,
    @SerializedName("positif") var kec_id : String,
    @SerializedName("sembuh") var sembuh : String,
    @SerializedName("meninggal") var meninggal : String

)