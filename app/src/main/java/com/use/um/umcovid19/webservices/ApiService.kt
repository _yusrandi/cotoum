package com.use.um.umcovid19.webservices

import com.use.um.umcovid19.models.Indonesia
import retrofit2.Call
import retrofit2.http.*

interface ApiService {


    @GET("indonesia")
    fun fetchIndonesiaData(): Call<List<Indonesia>>


}