package com.use.um.umcovid19.viewmodels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.*
import com.use.um.umcovid19.models.Kecamatan
import com.use.um.umcovid19.utils.Constants
import com.use.um.umcovid19.utils.SingleLiveEvent

class CaseViewModel : ViewModel(){

    companion object{
        const val TAG = "CaseViewModel"
        const val CASE_PATH = Constants.PROVINSI_PATH
    }

    private var cases = MutableLiveData<List<Kecamatan>>()
    private lateinit var mCaseRef: DatabaseReference
    private var states: SingleLiveEvent<CaseState> = SingleLiveEvent()

    private var list = ArrayList<Kecamatan>()

    fun init(): DatabaseReference{
        mCaseRef = FirebaseDatabase.getInstance().reference.child(CASE_PATH)
        return mCaseRef
    }

    fun fecthCaseData(context: Context, state: String, sub_admin: String){
        states.value = CaseState.IsLoading(true)
        mCaseRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p: DataSnapshot) {
                val children = p.children
                children.forEach {
                    val name = it.child("name").value.toString()

                    if(name == state){
                        val k = it.key.toString()
                        val child =  p.child(k).children

                        child.forEach {ch->

                            val name = ch.child("name").value.toString()
                            if(ch.key.toString() != "name")

                            if(name == sub_admin){

                                val ke = ch.key.toString()
                                val chil =  p.child(k).child(ke).children

                                Log.d(TAG, "sub_admin true $k, child ${p.child(k).child(ke).value}")

                                chil.forEach {chi->
                                    val name = chi.child("name").value.toString()
                                    val meninggal = chi.child("meninggal").value.toString()
                                    val positif = chi.child("positif").value.toString()
                                    val sembuh = chi.child("sembuh").value.toString()
                                    val kab_id = chi.child("kab_id").value.toString()
                                    val kec_id = chi.child("kec_id").value.toString()

                                    if(chi.key.toString() != "name" && chi.key.toString() != "prov_id"){
                                        val kec = Kecamatan(kec_id, kab_id, name, positif, sembuh, meninggal)
                                        list.add(kec)
                                        Log.d(TAG, "chi ${chi.key}, Name $name, Meninggal $meninggal")

                                        Log.d(TAG, "Kecamatan $kec")
                                    }

                                }


                            }

                        }


                    }else Log.d(TAG, "false")

                }

                states.value = CaseState.IsLoading(false)
                cases.postValue(list)

            }

            override fun onCancelled(error: DatabaseError) {
                states.value = CaseState.IsLoading(false)
                states.value = CaseState.IsError(error.message)

            }

        })
    }

    fun getCases() = cases
    fun getState() = states
}
sealed class CaseState {
    data class ShowToast(var message: String) : CaseState()
    data class IsLoading(var state: Boolean = false) : CaseState()
    data class IsError(var err: String?) : CaseState()
    data class IsSuccess(var msg: String) : CaseState()
    object Reset : CaseState()

}