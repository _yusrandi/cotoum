package com.use.um.umcovid19.chatbot.helpers

import android.util.Log
import com.google.cloud.dialogflow.v2.*
import java.lang.Exception
import com.use.um.umcovid19.chatbot.interfaces.BotReply




class SendMessageInBg(
    private var botReply: BotReply?,
    private var sessionName: SessionName?,
    private var sessionsClient: SessionsClient?,
    private var queryInput: QueryInput
) : CoroutinesAsyncTask<Void, Void, DetectIntentResponse>() {

    private val TAG = "SendMessageInBg"


    override fun doInBackground(vararg params: Void?): DetectIntentResponse? {
        Log.d(TAG, "doInBackground: ")

        try {
            val detectIntentRequest: DetectIntentRequest = DetectIntentRequest.newBuilder()
                .setSession(sessionName.toString())
                .setQueryInput(queryInput)
                .build()
            return sessionsClient!!.detectIntent(detectIntentRequest)
        } catch (e: Exception) {
            Log.d(TAG, "doInBackground: " + e.message)
            e.printStackTrace()
        }
        return null
    }

    override fun onPostExecute(result: DetectIntentResponse?) {
        Log.d(TAG, "onPostExecute: $result")
        botReply!!.callback(result)
    }

}