package com.use.um.umcovid19.models

import com.google.firebase.database.IgnoreExtraProperties


@IgnoreExtraProperties
data class Message(
  var isReceived: Boolean? = false,
  var message: String? = "",
){

}