package com.use.um.umcovid19.viewmodels

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.*
import com.use.um.umcovid19.models.Users
import com.use.um.umcovid19.utils.SingleLiveEvent
import java.util.HashMap


class MainViewModel : ViewModel(){

    companion object{
        const val TAG = "MainViewModel"
        const val USERS_PATH = "Users"
    }

    private var users = MutableLiveData<List<Users>>()
    private lateinit var mUserRef: DatabaseReference
    private var state : SingleLiveEvent<MainState> = SingleLiveEvent()

    fun init(): DatabaseReference{
        mUserRef = FirebaseDatabase.getInstance().reference.child(USERS_PATH)
        return mUserRef
    }

    fun createUser(){
        state.value = MainState.isLoading(true)

        val uid = java.util.UUID.randomUUID().toString()

        val userMap = HashMap<String, Any?>()
        userMap["name"] = "yusrandi"
        userMap["email"] = "yusrandi@ac.id"
        userMap["time"] = ServerValue.TIMESTAMP

        mUserRef.child(uid).setValue(userMap).addOnCompleteListener {
            state.value = MainState.isLoading(false)

            if(it.isSuccessful)
                state.value = MainState.isSuccess("Success")
            else
                state.value = MainState.isError("Failed ")

        }
    }
    fun fecthUserData(context: Context):LiveData<MutableList<Users>>{
        val mutableData = MutableLiveData<MutableList<Users>>()
        mUserRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(p: DataSnapshot) {
                val children = p.children
                children.forEach {
                    Log.d(TAG, it.toString())
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }

        })
        return mutableData
    }

    fun getState() = state

    sealed class MainState{
        data class showToast(var message : String) : MainState()
        data class isLoading(var state : Boolean = false) : MainState()
        //    data class ProductValidation(var categorie_id : String? = null, var content : String? = null) : ProductState()
        data class isError(var err : String?) : MainState()
        data class isSuccess(var msg : String?) : MainState()
        object Reset : MainState()
    }

}