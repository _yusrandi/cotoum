package com.use.um.umcovid19.models

data class Tips(val id:Int = 0,
                val title:String="DEFAULT TITLE",
                val desc:String="DEFAULT DESCRIPTION")