package com.use.um.umcovid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.getCredential
import com.google.firebase.database.FirebaseDatabase
import com.use.um.umcovid19.utils.Constants
import kotlinx.android.synthetic.main.activity_auth.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class AuthActivity : AppCompatActivity() {

    private var verificationCodeBySystem:String = ""
    private var phoneNumber:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        phoneNumber = intent.getStringExtra("no").toString()
        sendVerificationCodeToUser(phoneNumber)
        auth_btn_verify.setOnClickListener {
            verifyCode(etCodeAuth.text.toString())
        }
    }

    private fun sendVerificationCodeToUser(phoneNumber: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+62$phoneNumber", // Phone number to verify
            60, // Timeout duration
            TimeUnit.SECONDS, // Unit of timeout
            TaskExecutors.MAIN_THREAD, // Activity (for callback binding)
            callbacks
        ) // OnVerificationStateChangedCallbacks
    }

    private val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)

            verificationCodeBySystem = p0


        }
        override fun onVerificationCompleted(p0: PhoneAuthCredential) {

            val code = p0.smsCode
            if (code != null) {
                auth_progress.visibility = View.VISIBLE
                verifyCode(code)
                etCodeAuth.setText(code)
            }

        }

        override fun onVerificationFailed(p0: FirebaseException) {
            showToast(p0.message.toString())
        }


    }

    private fun verifyCode(codeByUser: String) {
        val credential = getCredential(verificationCodeBySystem, codeByUser)
        signInTheUserbyCreadetial(credential)
    }

    private fun signInTheUserbyCreadetial(credential: PhoneAuthCredential) {
        val firebaseAuth = FirebaseAuth.getInstance()

        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if(it.isSuccessful) {
                val currentUserUid = FirebaseAuth.getInstance().currentUser!!.uid
                saveUserToDatabase(currentUserUid)
            } else showToast(it.exception!!.message.toString())
        }
    }
    private fun saveUserToDatabase(uid:String){
       val  mDatabase = FirebaseDatabase.getInstance().reference.child(Constants.USERS_PATH).child(uid)

        val sdf = SimpleDateFormat("dd-mm-yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())

        val userMap = HashMap<String, Any?>()
        userMap["name"] = "Default"
        userMap["kadar"] = "0"
        userMap["suhu"] = "0"
        userMap["name"] = "Default"
        userMap["phone"] = phoneNumber
        userMap["time"] = currentDate

        mDatabase.setValue(userMap).addOnCompleteListener {
            if(it.isSuccessful) startApp() else showToast(it.exception!!.message.toString())
        }
    }
    private  fun startApp(){
        startActivity(Intent(this, HomeActivity::class.java ).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }).also {
            finish()
        }
    }
    private fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}
