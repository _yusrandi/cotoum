package com.use.um.umcovid19

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.auth.FirebaseAuth
import com.stephentuso.welcome.WelcomeHelper
import com.use.um.umcovid19.adapters.MainAdapter
import com.use.um.umcovid19.viewmodels.IndonesiaViewModel
import com.use.um.umcovid19.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object{
        const val TAG = "MainActivity"
    }

    private lateinit var indonesiaViewModel: IndonesiaViewModel
    private lateinit var mAuth: FirebaseAuth

    private lateinit var adapter: MainAdapter

    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }

    private lateinit var welcomeScreen: WelcomeHelper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar_layout.title = title

//        indonesiaViewModel = ViewModelProviders.of(this).get(IndonesiaViewModel::class.java)
        mAuth = FirebaseAuth.getInstance()



        welcomeScreen = WelcomeHelper(this, WelcomeScreen::class.java)
        welcomeScreen.show(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        welcomeScreen.onSaveInstanceState(outState);
    }

    private fun getIndonesiasData() {
        indonesiaViewModel.getState().observer(this, Observer {
            Log.d(TAG, "Response $it")
        })
        indonesiaViewModel.getIndonesias().observe(this, Observer {
            Log.d(TAG, "Response $it")
            it.forEach {
                Log.d(TAG, "Response ${it.dirawat}")

            }
        })


    }

    private fun toastMsg(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    override fun onResume() {
        super.onResume()

//        indonesiaViewModel.fecthIndonesiaData()

    }

    override fun onStart() {
        super.onStart()
        if(mAuth.currentUser == null) sendToStart()
    }

    private fun sendToStart() {
        startActivity(Intent(this@MainActivity, SignUpActivity::class.java))
        finish()
    }
}
