package com.use.um.umcovid19.models

import com.google.gson.annotations.SerializedName

data class Indonesia(
    @SerializedName("name") var name : String,
    @SerializedName("positif") var positif : String,
    @SerializedName("sembuh") var sembuh : String,
    @SerializedName("meninggal") var meninggal : String,
    @SerializedName("dirawat") var dirawat : String

)