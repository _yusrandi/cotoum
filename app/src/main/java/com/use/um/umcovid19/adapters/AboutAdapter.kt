package com.use.um.umcovid19.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.use.um.umcovid19.DetailTipsActivity
import com.use.um.umcovid19.R
import com.use.um.umcovid19.models.Tips
import kotlinx.android.synthetic.main.item_tips.view.*

class AboutAdapter(private val context: Context): RecyclerView.Adapter<AboutAdapter.AboutViewlHolder>() {

    companion object{
        const val TAG = "TipsAdapter"
    }
    private var dataList = mutableListOf<Tips>()

    fun setList(data: MutableList<Tips>){
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutViewlHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_tips, parent, false)

        return AboutViewlHolder(view)
    }

    override fun onBindViewHolder(holder: AboutViewlHolder, position: Int) {
        holder.bindView(dataList[position])
    }

    override fun getItemCount(): Int {
        return if(dataList.size>0) dataList.size else 0
    }

    inner class AboutViewlHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bindView(tips: Tips){
//            Glide.with(context).load(users.img).into(itemView.item_main_img)
            val tipsSplit = tips.desc.split("//")
            itemView.item_tips_title.text = tips.title
            itemView.item_tips_desc.text = tipsSplit[0]+" ..."

            Log.d(TAG, "Data ${tips.id}")
            itemView.item_tips_read.setOnClickListener {
                context.startActivity(Intent(context, DetailTipsActivity::class.java).apply {
                    putExtra("id", tips.id)
                    putExtra("status", "Tentang")
                })
            }


        }
    }
}