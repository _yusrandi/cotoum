package com.use.um.umcovid19.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.use.um.umcovid19.HomeActivity
import java.io.IOException
import java.util.*

class UserLocationListener(ctx: Context, uid: String) : GoogleApiClient.ConnectionCallbacks,
    LocationListener,
    GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val TAG = "UserLocationListener"
    }

    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private val UPDATE_INTERVAL: Long = 0
    private val FASTEST_UPDATE_INTERVAL = 1000 * 60 * 1.toLong()
    private val MAX_WAIT_TIME = UPDATE_INTERVAL * 5

    private lateinit var mUserRef: DatabaseReference
    private var context: Context
    private var userId : String

    init {
        context = ctx
        userId = uid
        mUserRef = FirebaseDatabase.getInstance().reference.child(Constants.USERS_PATH)
        buildGoogleApiClient()
    }

    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context)
            .addConnectionCallbacks(this)
            .enableAutoManage(context as FragmentActivity, this)
            .addApi(LocationServices.API)
            .build()
        createLocationRequest()
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL

        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationRequest.maxWaitTime = MAX_WAIT_TIME
    }

    protected fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        val pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient, mLocationRequest, this
        )
        Log.d(HomeActivity.TAG, "Location update started ..............: ")
    }


    fun getUserAdress(p0: Location): String{
        var userAdres: String = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address>

        try {
            addresses = geocoder.getFromLocation(p0.latitude, p0.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                val city: String = addresses[0].locality
                val state: String = addresses[0].adminArea
                val sub_admin: String = addresses[0].subAdminArea
                val country: String = addresses[0].countryName
                val postalCode: String = addresses[0].postalCode
                val knownName: String = addresses[0].featureName
                Log.d(HomeActivity.TAG, addresses[0].toString())
                userAdres = "$city, $sub_admin"
                Log.d(HomeActivity.TAG, "Adress $address")
            }
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return userAdres
    }
    override fun onConnected(bundle: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(p: Int) {
        Log.d(TAG, "onConnectionSuspended")
    }

    override fun onLocationChanged(location: Location) {
        Log.d(TAG, "onLocationChanged LatLang ${location.latitude}")
        mUserRef.child(userId).child("location").setValue("${location.latitude},${location.longitude}").addOnCompleteListener {
            if(it.isComplete) Log.d(TAG, "Succes update location") else Log.d(TAG, "UnSucces update location")
        }

    }

    override fun onConnectionFailed(p: ConnectionResult) {
        Log.d(TAG, "onConnectionFailed")

    }

}