package com.use.um.umcovid19.viewmodels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.*
import com.use.um.umcovid19.models.Users
import com.use.um.umcovid19.utils.SingleLiveEvent
import java.util.HashMap


class UsersViewModel : ViewModel(){

    companion object{
        const val TAG = "MainViewModel"
        const val USERS_PATH = "Users"
    }

    private var users = MutableLiveData<List<Users>>()
    private lateinit var mUserRef: DatabaseReference
    private var state : SingleLiveEvent<UserState> = SingleLiveEvent()

    fun init(): DatabaseReference{
        mUserRef = FirebaseDatabase.getInstance().reference.child(USERS_PATH)
        return mUserRef
    }

    fun createUser(){
        state.value = UserState.isLoading(true)

        val uid = java.util.UUID.randomUUID().toString()

        val userMap = HashMap<String, Any?>()
        userMap["name"] = "yusrandi"
        userMap["email"] = "yusrandi@ac.id"
        userMap["time"] = ServerValue.TIMESTAMP

        mUserRef.child(uid).setValue(userMap).addOnCompleteListener {
            state.value = UserState.isLoading(false)

            if(it.isSuccessful)
                state.value = UserState.isSuccess("Success")
            else
                state.value = UserState.isError("Failed ")

        }
    }
    fun updateSuhu(uid:String,suhu:String, kadar:String){
        state.value = UserState.isLoading(true)


        val userMap = HashMap<String, Any?>()
        userMap["suhu"] = suhu
        userMap["kadar"] = kadar

        mUserRef.child(uid).updateChildren(userMap).addOnCompleteListener {
            state.value = UserState.isLoading(false)

            if(it.isSuccessful)
                state.value = UserState.isSuccess("Success")
            else
                state.value = UserState.isError("Failed ")

        }
    }

    fun fecthUserData(context: Context){

        var listUser = ArrayList<Users>()

        mUserRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(p: DataSnapshot) {
                val children = p.children
                children.forEach {
                    Log.d(TAG, it.toString())
                    val user = it.getValue(Users::class.java)
                    val user1 = user?.copy(key = it.key!!)
                    listUser.add(user1!!)
                }
                users.postValue(listUser)
            }
            override fun onCancelled(error: DatabaseError) {

            }

        })

    }

    fun getState() = state
    fun getUsers() = users

    sealed class UserState{
        data class showToast(var message : String) : UserState()
        data class isLoading(var state : Boolean = false) : UserState()
        //    data class ProductValidation(var categorie_id : String? = null, var content : String? = null) : ProductState()
        data class isError(var err : String?) : UserState()
        data class isSuccess(var msg : String?) : UserState()
        object Reset : UserState()
    }

}