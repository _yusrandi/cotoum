package com.use.um.umcovid19.models

data class Users(val location:String ="DEFAULT LOCATION",
                 val name:String="DEFAULT NAME",
                 val phone:String="DEFAULT PHONE",
                 val time:String="DEFAULT TIME",
                 val key:String="DEFAULT KEY")

