package com.use.um.umcovid19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.use.um.umcovid19.adapters.MessageAdapter
import com.use.um.umcovid19.models.Message
import kotlinx.android.synthetic.main.activity_chat_bot.*
import kotlinx.android.synthetic.main.toolbar_back.*
import java.util.*
import android.widget.Toast
import com.google.api.gax.core.FixedCredentialsProvider

import com.google.auth.oauth2.ServiceAccountCredentials

import com.google.auth.oauth2.GoogleCredentials

import com.google.common.collect.Lists
import java.io.InputStream
import com.use.um.umcovid19.chatbot.helpers.SendMessageInBg

import com.google.cloud.dialogflow.v2.*
import com.use.um.umcovid19.chatbot.interfaces.BotReply


class ChatBotActivity : AppCompatActivity(), BotReply {

    companion object{
        const val TAG = "ChatBotActivity"
    }
    private var messages = ArrayList<Message>()
    private lateinit var messageAdapter: MessageAdapter

    //Chat Bot DialogFlow
    private var sessionName: SessionName? = null
    private var sessionsClient: SessionsClient? = null
    private val uuid = UUID.randomUUID().toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_bot)

        toolbar_back_title.text = "Chat Bot"

        setUpBot()
        loadMessages()

        chat_layout_send.setOnClickListener {
            val message: String = chat_msg.text.toString()
            if (!message.isEmpty()) {
                messages.add(Message(false, message))
                chat_msg.setText("")
                sendMessageToBot(message)
                Objects.requireNonNull(chat_rv.adapter!!.notifyDataSetChanged())
                Objects.requireNonNull(chat_rv.layoutManager)?.scrollToPosition(messages.size - 1)
            } else {
                Toast.makeText(this@ChatBotActivity, "Please enter text!", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun loadMessages() {
//        messages.add(Message(false, "Bagaimana caranya agar rumah saya aman dari penularan virus corona?"))
//        messages.add(Message(true, "Pastikan keluarga menerapkan perilaku bersih, yaitu sering cuci tangan pakai sabun dan air mengalir, di antaranya saat mereka tiba di rumah, sebelum makan dan setelah menggunakan kamar mandi. Secara teratur bersihkan permukaan benda-benda yang sering tersentuh tangan, seperti saklar lampu atau gagang pintu dengan lap, tisu atau carian disinfektan. Cuci baju yang telah terpakai dengan deterjen dengan takaran sesuai label."))
//
//        messages.add(Message(false, "Apakah ada hal yang tidak boleh saya lakukan?"))
//        messages.add(Message(true, "Tindakan-tindakan berikut TIDAK efektif melawan COVID-19 dan bahkan bisa berbahaya: Merokok Mengenakan lebih dari satu masker * Meminum antibiotik Apa pun keadaannya, jika Anda mengalami demam, batuk dan kesulitan bernapas, segeralah mencari pertolongan medis untuk mengurangi risiko terkena infeksi yang lebih parah dan sampaikan riwayat perjalanan Anda baru-baru ini kepada tenaga kesehatan. sumber: WHO"))

        messageAdapter = MessageAdapter(messages, this@ChatBotActivity)
        chat_rv.apply {
            layoutManager = LinearLayoutManager(this@ChatBotActivity, LinearLayoutManager.VERTICAL, false)
            adapter = messageAdapter
            setHasFixedSize(true)

        }
    }
    private fun setUpBot(){
        try {
            val stream: InputStream = this.resources.openRawResource(R.raw.credential)
            val credentials: GoogleCredentials = GoogleCredentials.fromStream(stream)
                .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"))
            val projectId = (credentials as ServiceAccountCredentials).projectId

            val settingsBuilder: SessionsSettings.Builder = SessionsSettings.newBuilder()
            val sessionsSettings: SessionsSettings = settingsBuilder.setCredentialsProvider(
                FixedCredentialsProvider.create(credentials)
            ).build()
            sessionsClient = SessionsClient.create(sessionsSettings)
            sessionName = SessionName.of(projectId, uuid)

            Log.d(TAG, "projectId : $projectId")
        }catch (e: Exception){
            Log.e(TAG, "setUpBot : ${e.message}")
        }
    }
    private fun sendMessageToBot(message: String) {
        chat_layout_send_progress.visibility = View.VISIBLE
        val input: QueryInput = QueryInput.newBuilder()
            .setText(TextInput.newBuilder().setText(message).setLanguageCode("en-US")).build()
        SendMessageInBg(this, sessionName, sessionsClient, input).execute()
    }

    override fun callback(returnResponse: DetectIntentResponse?) {
        if (returnResponse != null) {
            val botReply = returnResponse.queryResult.fulfillmentText
            Log.e(TAG, "callback , returnResponse!=null $botReply")
            if (botReply.isNotEmpty()) {
                runOnUiThread {
                    chat_layout_send_progress.visibility = View.GONE

                    messages.add(Message(true, botReply))
                    messageAdapter.notifyDataSetChanged()
                    Objects.requireNonNull(chat_rv.layoutManager)?.scrollToPosition(messages.size - 1)
                }

            } else {
                Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show()
            }
        } else {
            Log.e(TAG, "callback , returnResponse=null")

            Toast.makeText(this, "failed to connect!", Toast.LENGTH_SHORT).show()
        }
    }

}