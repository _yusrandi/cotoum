package com.use.um.umcovid19

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.use.um.umcovid19.data.DataAbout
import com.use.um.umcovid19.data.DataTips
import com.use.um.umcovid19.models.Tips
import kotlinx.android.synthetic.main.activity_detail_tips.*
import kotlinx.android.synthetic.main.toolbar_back.*

class DetailTipsActivity : AppCompatActivity() {

    companion object{
        const val TAG = "DetailTipsActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tips)

        val id = intent.getIntExtra("id", 0)
        val status = intent.getStringExtra("status")

        toolbar_back_title.text = status

        Log.d(TAG, "id $id")

        var data = mutableListOf<Tips>()
        if(status == "Tips") data = DataTips().getDataTips() else data = DataAbout().getDataAbout()

        detail_tips_title.text = data[id.toInt()].title
        detail_tips_desc.text = data[id].desc.replace("//", "")

    }
}
