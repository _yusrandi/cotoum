package com.use.um.umcovid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.use.um.umcovid19.adapters.TipsAdapter
import com.use.um.umcovid19.data.DataTips
import kotlinx.android.synthetic.main.activity_tips.*

class TipsActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val TAG = "TipsActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tips)

        Log.d(TAG, "onCreate")
        tips_navigation.selectedItemId = R.id.tips
        tips_navigation.setOnNavigationItemSelectedListener(this)



        initRecyclerView()
    }

    private fun initRecyclerView() {

        val adapter = TipsAdapter(this)
        tips_rv.layoutManager = LinearLayoutManager(this)
        tips_rv.adapter = adapter

        val data = DataTips()
        Log.d(TAG, "Data Tips ${data.getDataTips()}")

        adapter.setList(data.getDataTips())
        adapter.notifyDataSetChanged()

    }

    private fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.main -> {
                startActivity(Intent(applicationContext, HomeActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.tips -> {

                true
            }
            R.id.info -> {
                startActivity(Intent(applicationContext, InfoActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.maps -> {
                startActivity(Intent(applicationContext, MapsActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            else -> false
        }
    }

}
