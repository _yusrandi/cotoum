package com.use.um.umcovid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.auth.FirebaseAuth
import com.use.um.umcovid19.viewmodels.UsersViewModel
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.toolbar_back.*

class SettingActivity : AppCompatActivity() {

    private val userViewModel by lazy { ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(UsersViewModel::class.java) }
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        toolbar_back_title.text = "Setting"
        setting_location.setText(intent.getStringExtra("loc"))

        mAuth = FirebaseAuth.getInstance()
        userViewModel.init()

        setting_submit.setOnClickListener { updateUser() }
        userViewModel.getState().observe(this, Observer {
            handleUIState(it)
        })

    }

    private fun updateUser() {
        val suhu = setting_suhu.text.toString()
        val kadar = setting_kadar.text.toString()

        if(suhu.isEmpty() && kadar.isEmpty()) showToast("Please fill all field") else{
            userViewModel.updateSuhu(mAuth.uid.toString(), suhu, kadar)
        }
    }

    private fun handleUIState(it: UsersViewModel.UserState) {
        when (it) {

            is UsersViewModel.UserState.isError -> {
                isLoading(false)
                showToast(it.err.toString())
            }
            is UsersViewModel.UserState.showToast -> showToast(it.message)

            is UsersViewModel.UserState.isSuccess -> {
                isLoading(false)
                startActivity(Intent(this, HomeActivity::class.java).also { finish() })
            }
            is UsersViewModel.UserState.isLoading -> isLoading(it.state)
            else -> showToast("Undefined")
        }
    }

    private fun isLoading(state: Boolean) {

        if (state) {
            setting_loading.visibility = View.VISIBLE
        } else {
            setting_loading.visibility = View.GONE
        }
    }

    private fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}