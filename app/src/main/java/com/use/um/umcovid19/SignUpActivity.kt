package com.use.um.umcovid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)


        signup_next.setOnClickListener {

            val noUser = etNoSignIn.text.toString()

            if(noUser.isEmpty()){
                etNoLayoutSignIn.error = "Harap Masukkan Nomor Anda"
            }else if(noUser.length < 10){
                etNoLayoutSignIn.error = "Harap Masukkan Nomor Yang Valid"
            }else{
                startActivity(Intent(this, AuthActivity::class.java).apply {
                    putExtra("no", noUser)
                }).also { finish() }
            }

        }
    }
}
