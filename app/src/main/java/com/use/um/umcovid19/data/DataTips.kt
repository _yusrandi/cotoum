package com.use.um.umcovid19.data

import com.use.um.umcovid19.models.Tips

class DataTips {
    private var dataList = mutableListOf<Tips>()
    init {
        initData()
    }
    private fun initData(){
        dataList.add(Tips(0, "Kapan saya harus memeriksakan diri?", "\t\t\tApabila Anda dihubungi oleh petugas kesehatan, sebagai hasil analisa aplikasi PeduliLindungi, berarti Anda punya riwayat kontak dengan penderita COVID-19 positif, PDP, atau ODP. Maka silakan periksakan diri Anda.// \n\n" +
                "Selain itu, sesuai dengan prosedur yang sudah ditetapkan oleh pihak berwenang atau pemerintah, Anda juga dapat menghubungi 119 ext. 9 atau periksa ke Rumah Sakit rujukan jika: \n\n" +
                "1. Pernah berkunjung ke daerah endemis COVID-19 dalam 14 hari terakhir dan mengalami gejala seperti demam di atas 38°C, batuk, dan sesak nafas yang tidak kunjung membaik. \n" +
                "2. Pernah kontak langsung dengan pasien COVID-19 atau punya riwayat perjalanan ke daerah episentrum penyebaran virus, meski tak menunjukkan gejala, namun bisa saja Anda menjadi carrier coronavirus. \n"+
                "3. Mengalami gejala ringan-sedang yang mengarah ke COVID-19 diantaranya adalah demam, batuk, sesak napas yang tak kunjung reda, meski dirasa tidak ada riwayat kontak langsung dengan pasien atau berada di daerah episentrum."
                ))
        dataList.add(Tips(1, "Bagaimana cara mencegah penyebaran Coronavirus Disease (COVID-19)? ", "\t\t\tBagaimana cara mencegah penyebaran COVID-19? \n" +
                "Beberapa cara yang dapat dilakukan untuk mencegah penularan COVID-19 adalah:// \n" +
                "\n" +
                "1.Jaga kesehatan dan kebugaran Anda agar sistem imunitas/ kekebalan tubuh meningkat. \n" +
                "2. Cucilah tangan menggunakan sabun dan air mengalir, karena hal ini dapat membunuh virus yang ada di tangan kita. Hal ini murah dan mudah untuk dilakukan, untuk menghindari penularan dari tangan. Jika tidak ada air dan sabun, bisa menggunakan hand-sanitizer berbasis alkohol. \n" +
                "3. Upayakan menerapkan etika batuk dan bersin. Tutup hidung dan mulut Anda dengan tisu atau dengan lengan atas bagian dalam. \n" +
                "4. Jaga jarak saat bertemu dengan orang lain, sekurang-kurangnya satu meter, terutama dengan orang yang sedang menderita batuk, pilek, bersin, dan demam, karena saat seseorang terinfeksi penyakit saluran pernafasan seperti COVID-19, batuk/bersin dapat menghasilkan droplet yang mengandung virus. \n" +
                "5. Hindari menyentuh mata, hidung, dan mulut sebelum mencuci tangan. Karena mata, hidung, dan mulut dapat menjadi jalan masuk virus yang hinggap ke tangan yang belum dicuci. \n" +
                "6. Gunakan masker penutup hidung dan mulut ketika Anda sakit. \n" +
                "7. Buang tisu atau masker yang sudah digunakan ke tempat sampah kemudia cucilah tangan Anda. \n" +
                "8. Tunda perjalanan ke daerah yang terpapar dengan virus ini sampai ada informasi lebih lanjut. \n" +
                "9. Jauhi tempat-tempat keramaian atau berdiam diri di rumah selama waktu yang ditentukan oleh pihak berwenang."))
        dataList.add(Tips(2, "Jaga Jarak itu Penting! ", "\t\t\tBila penyebaran virus terjadi di lokasi tertentu, tindakan mengurangi kontak antarwarga pertama-tama dilakukan di lokasi-lokasi tersebut dan tidak langsung di tingkat nasional.\n" +
                "\n" +
                " Caranya adalah dengan melakukan hal-hal sebagai berikut://\n" +
                " 1. Hindari pertemuan besar (lebih dari 10 orang). \n" +
                " 2. Jaga jarak (1 meter atau lebih) dengan orang lain. \n" +
                " 3. Jangan pergi ke sarana kesehatan kecuali diperlukan. \n" +
                " 4. Bila Anda memiliki anggota keluarga atau kawan dirawat di rumah sakit, batasi pengunjung - terutama bila mereka anak-anak atau kelompok risiko tinggi (lanjut usia dan orang dengan penyakit yang dapat memperberat, misalnya gangguan jantung, diabetes dan penyakit kronis lainnya) \n" +
                " 5. Orang berisiko tinggi sebaiknya tetap di rumah dan menghindari pertemuan atau kegiatan lain yang dapat membuatnya terpapar virus, termasuk melakukan perjalanan \n" +
                " 6. Beri dukungan pada anggota keluarga (yang tidak tinggal di rumah Anda) ataupun tetangga yang terinfeksi tanpa harus bertemu langsung, misalnya melalui telepon ataupun aplikasi bertukar pesan lainnya. \n" +
                " 7. Ikuti panduan resmi di wilayah Anda yang bisa saja merubah rutinitas termasuk kegiatan sekolah atau pekerjaan. \n" +
                " 8. Ikuti perkembangan informasi karena situasi dapat berubah dengan cepat sesuai perkembangan penyakit dan penyebarannya."))
        dataList.add(Tips(3, "Cara tepat menggunakan masker ", "1. Sebelum memasang masker, cuci tangan pakai sabun dan air mengalir (minimal 20 detik) atau bila tidak tersedia, gunakan cairan pembersih tangan (minimal alkohol 60%). \n" +
                "2. Pasang masker menutupi //hidung, mulut, sampai dagu. Pastikan tidak ada sela antara wajah dan masker. \n" +
                "3. Jangan buka tutup masker. Jangan menyentuh masker. Bila tersentuh, cuci tangan pakai sabun dan air mengalir (minimal 20 detik) atau bila tidak tersedia, gunakan cairan pembersih tangan (minimal alkohol 60%). \n" +
                "4. Ganti masker yang basah atau lembab dengan masker baru. Masker medis hanya boleh digunakan satu kali saja. Masker kain dapat digunakan berulang kali, namun harus dicuci terlebih dahulu jika ingin menggunakannya lagi. \n" +
                "5. Untuk membuka masker: lepaskan dari belakang. Jangan sentuh bagian depan masker. Untuk masker satu kali pakai, buang segera di tempat sampah tertutup atau kantong plastik. Untuk masker kain, segera cuci dengan deterjen. Untuk memasang masker baru, ikuti poin pertama."))
    }

    fun getDataTips() = dataList
}