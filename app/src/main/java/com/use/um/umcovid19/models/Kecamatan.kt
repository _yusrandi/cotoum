package com.use.um.umcovid19.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Kecamatan(
    var id : String = "",
    var kab_id : String = "",
    var name : String = "",
    var positif : String = "",
    var sembuh : String = "",
    var meninggal : String = ""
){
}