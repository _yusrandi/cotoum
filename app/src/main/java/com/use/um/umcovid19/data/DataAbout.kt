package com.use.um.umcovid19.data

import com.use.um.umcovid19.models.Tips

class DataAbout {
    private var dataList = mutableListOf<Tips>()
    init {
        initData()
    }
    private fun initData(){
        dataList.add(Tips(0, "Definisi COTO UM", "\t\t\tAplikasi COTO UM (selanjutnya dapat disebut COTO UM) adalah aplikasi yang digunakan oleh Instansi kampus UM Malang untuk kepentingan pelacakan dan penghentian penyebaran Coronavirus Disease (COVID-19) di wilayah UM.// COTO UM melindungi diri Anda, keluarga Anda, dan orang terdekat lainnya, serta menghentikan penyebaran COVID-19 dengan mengandalkan partisipasi masyarakat untuk saling membagikan data lokasinya saat bepergian agar penelusuran riwayat kontak dengan penderita COVID-19 dapat dilakukan."
                ))
        dataList.add(Tips(1, "Cara Kerja COTO UM ", "1. Pada saat pengguna mengunduh COTO UM, aplikasi akan meminta persetujuan pengguna untuk mengaktifkan bluetooth pada ponselnya. \n" +
                "2. Dengan kondisi bluetooth aktif, maka// secara berkala aplikasi akan melakukan identifikasi ponsel pengguna COTO UM lainnya, yang berada di radius bluetooth, dengan menggunakan scanning bluetooth untuk merekam lokasi dan waktu kontaknya. \n" +
                "3. Ponsel-ponsel yang berdekatan kemudian akan saling merekam ID anonim masing-masing. Sehingga, apabila ada seseorang yang dinyatakan sakit oleh petugas kesehatan (bukan oleh aplikasi) dan diinputkan ke sistem database, maka sistem akan memfilter ID-ID anonim lain. \n" +
                "4. Kemudian, petugas kesehatan dapat menghubungi pengguna ponsel lain yang ada dalam riwayat kontak tersebut untuk mengingatkan risiko kontak yang pernah terjadi. \n" +
                "5. Informasi riwayat kontak ini yang akan dipakai untuk melakukan tracing saat salah satu dari pengguna dinyatakan positif COVID-19. \n" +
                "6. Sehingga, semakin banyak partisipasi masyarakat yang menggunakan aplikasi ini, akan semakin besar pula partisipasi COTO UM membantu pemerintah dalam melakukan tracing dan tracking. \n" +
                "COTO UM hanya akan memberikan notifikasi jika: \n" +
                "1. Anda teridentifikasi berada di keramaian, yaitu berada di tempat yang sama dengan beberapa pengguna lain yang mengaktifkan aplikasi PeduliLindungi dalam waktu yang cukup lama. \n" +
                "2. Anda masuk ke suatu zona tertentu: \n" +
                "     Zona Merah yaitu area atau kelurahan yang sudah terdata bahwa ada orang yang terinfeksi positif COVID-19 atau ada Pasien Dalam Pengawasan (PDP). Beranda Tips Teledokter Tentang\n" +
                "     Zona Kuning yaitu area atau kelurahan yang sudah terdata bahwa ada Orang Dalam Pemantauan (ODP). \n" +
                "     Zona Hijau yaitu area atau kelurahan yang sudah terdata bahwa tidak ada Orang Dalam Pemantauan (ODP), Pasien Dalam Pengawasan (PDP), maupun yang terinfeksi positif COVID-19. \n" +
                "     \n" +
                "Anda berstatus dalam karantina mandiri, namun Anda keluar dari zona karantina/isolasi."))
        dataList.add(Tips(2, "Penjelasan tentang Keamanan Penggunaan Bluetooth", "\t\t\tBluetooth yang menyala terus dan terbuka memang seolah-olah rawan untuk diserang oleh attacker. Tetapi di sistem default yang ada, ponsel akan meminta izin kepada Anda apabila ada ponsel lain yang ingin bertukar data melalui bluetooth. Jadi,//selama Anda membiarkan kondisi default tersebut aktif, penggunaan bluetooth pada PeduliLindungi relatif aman. \n" +
                "Sekarang ini terdapat juga informasi bahwa ada kemungkinan attacker mengirim data ke ponsel yang bluetoothnya aktif tanpa meminta izin pengguna. Dengan teknologi security saat ini maka hal tersebut belum pernah terjadi (tidak dapat dilakukan). Masing-masing operating system (Android dan iOS) juga selalu melakukan pembaruan dalam security bluetooth-nya, sehingga yang perlu Anda lakukan adalah memastikan bahwa Bluetooth Anda merupakan versi terbaru. Sejauh ini, terbukti belum ada laporan serangan yang terjadi pada aplikasi sejenis lainnya yang mirip dengan aplikasi \n" +
                "COTO UM (seperti aplikasi yang dipakai oleh pemerintah Indonesia, Singapore dan China). Dengan demikian, bluetooth memiliki keamanan yang baik. COTO UM secara terus menerus bekerja dan mengawasi untuk memastikan agar aplikasi ini tetap aman digunakan."))
        dataList.add(Tips(3, "Cara pengunaan aplikasi dan hemat baterai", "\t\t\tPastikan aplikasi COTO UM selalu dalam keadaan terbuka (open) agar aplikasi dapat terus berfungsi. \n" +
                "Untuk mengakses \"Low Power Mode/Battery Saver\" pada Android 5.0 (lollipop) ke atas, \\melalui \"Setting\": \n" +
                "1. Masuk ke menu \"Pengaturan/Setting\" lalu masuk ke pilihan \"Device/ Perangkat\" atau \"Baterai/Battery\". \n" +
                "2. Geser tombol sampai menyala untuk mengaktifkan \"Low Power Mode\"."))
        dataList.add(Tips(4, "Sumber informasi dan imbauan", "\t\t\tSumber informasi dan imbauan yang terdapat di aplikasi COTO UM berasal dari Kementerian Kesehatan, Kementerian Kominfo, Situs Resmi COVID-19 Indonesia dan Organisasi Kesehatan Dunia (WHO). //Untuk mengakses sumber informasi dan imbauan, Anda bisa mengklik referensi yang terdapat di bagian paling bawah dari setiap artikel."))
        dataList.add(Tips(5, "Hukum yang Berlaku", "\t\t\tKetentuan ini tunduk pada hukum Negara Republik Indonesia. Dalam hal terdapat perbedaan penafsiran/perselisihan maka diupayakan untuk dapat diselesaikan secara musyawarah mufakat. //Apabila dalam waktu 90 (sembilan puluh) hari kalender tidak ada kesepakatan maka Anda setuju untuk memilih penyelesaian perselisihan pada Badan Arbitrase Nasional Indonesia (BANI) di Jakarta."))
        dataList.add(Tips(6, "Kontak Kami", "\t\t\tJika Anda mengalami kendala atau ada pertanyaan lainnya terkait penggunaan aplikasi COTO UM, Anda bisa mengirimkan e-mail ke covid19monum@gmail.com\\"))
        dataList.add(Tips(7, "Versi Aplikasi", "\t\t\tSaat ini versi aplikasi COTO UM yaitu 1.0.0//"))
    }

    fun getDataAbout() = dataList
}