package com.use.um.umcovid19.utils

import android.content.Context
import android.graphics.Color
import cn.pedant.SweetAlert.SweetAlertDialog

class MessageHandler(cx: Context) {

    private var ctx = cx;
    private lateinit var loadingDialog: SweetAlertDialog
    private lateinit var errorLoading: SweetAlertDialog
    private lateinit var successLoading: SweetAlertDialog


    fun closeAlert() {
        loadingDialog.dismissWithAnimation()
    }

    fun alertLoading() {

        loadingDialog = SweetAlertDialog(ctx, SweetAlertDialog.PROGRESS_TYPE)
        loadingDialog.progressHelper.barColor = Color.parseColor("#013838")
        loadingDialog.titleText = "Loading"
        loadingDialog.setCancelable(true)
        loadingDialog.show()


    }

    fun alertError(msg: String) {
        errorLoading = SweetAlertDialog(ctx, SweetAlertDialog.ERROR_TYPE)
        errorLoading.titleText = "Terjadi Kesalahan !"
        errorLoading.contentText = msg
        errorLoading.setCancelable(false)
        errorLoading.setConfirmClickListener { errorLoading.dismissWithAnimation() }
        errorLoading.show()

    }

    fun alertConfirm(title: String, msg: String, msgSuccess: String, what: Int) {
        SweetAlertDialog(ctx, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Tambah item") //
            .setContentText("apakah anda yakin tambah ke keranjang ?")
            .setConfirmText("Ya, yakin")
            .setConfirmClickListener { sDialog ->
                alertSuccess(msgSuccess, what)
                sDialog.dismissWithAnimation()
            }
            .setCancelButton("Batal") { ssDialog -> ssDialog.dismissWithAnimation() }
            .show()
    }

    fun alertSuccess(msg: String, what: Int) {
        successLoading = SweetAlertDialog(ctx, SweetAlertDialog.SUCCESS_TYPE)
        successLoading.titleText = "Berhasil"
        successLoading.contentText = msg

        successLoading.setConfirmClickListener(SweetAlertDialog.OnSweetClickListener {
            successLoading.dismissWithAnimation()


        })

        successLoading.show()

    }

}