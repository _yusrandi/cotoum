package com.use.um.umcovid19.models

import com.google.gson.annotations.SerializedName

data class Provinsi(
    @SerializedName("id") var id : String = "",
    @SerializedName("name") var name : String = "",
    @SerializedName("kabupaten") var kabupaten : List<Kabupaten> = emptyList()


)