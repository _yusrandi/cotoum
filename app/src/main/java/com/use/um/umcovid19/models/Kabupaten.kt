package com.use.um.umcovid19.models

import com.google.gson.annotations.SerializedName

data class Kabupaten(
    @SerializedName("id") var id : String,
    @SerializedName("prov_id") var prov_id : String,
    @SerializedName("name") var name : String,
    @SerializedName("kecamatan") var kecamatan : List<Kecamatan>


)