package com.use.um.umcovid19

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.use.um.umcovid19.adapters.AboutAdapter
import com.use.um.umcovid19.data.DataAbout
import kotlinx.android.synthetic.main.activity_info.*
import kotlinx.android.synthetic.main.toolbar.*

class InfoActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val TAG = "InfoActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        toolbar_title.text = "Tentang"
        info_navigation.selectedItemId = R.id.info
        info_navigation.setOnNavigationItemSelectedListener(this)

        initRecyclerView()


    }

    private fun initRecyclerView() {
        val adapter = AboutAdapter(this)
        info_rv.layoutManager = LinearLayoutManager(this)
        info_rv.adapter = adapter

        val data = DataAbout()
        Log.d(TipsActivity.TAG, "Data Tips ${data.getDataAbout()}")

        adapter.setList(data.getDataAbout())
        adapter.notifyDataSetChanged()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.main-> {
                startActivity(Intent(applicationContext, HomeActivity::class.java)).also { finish() }
                overridePendingTransition(0,0)
                true
            }
            R.id.tips-> {
                startActivity(Intent(applicationContext, TipsActivity::class.java)).also { finish() }
                overridePendingTransition(0,0)
                true
            }
            R.id.info-> {
                true
            }
            R.id.maps -> {
                startActivity(Intent(applicationContext, MapsActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            else -> false
        }
    }
}
