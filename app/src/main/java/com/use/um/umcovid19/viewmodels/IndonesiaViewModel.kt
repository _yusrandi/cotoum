package com.use.um.umcovid19.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.use.um.umcovid19.models.Indonesia
import com.use.um.umcovid19.utils.SingleLiveEvent
import com.use.um.umcovid19.webservices.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IndonesiaViewModel : ViewModel() {
    companion object{
        const val TAG = "IndonesiaViewModel"
    }
    private var indonesias = MutableLiveData<List<Indonesia>>()
    private var state: SingleLiveEvent<IndonesiaState> = SingleLiveEvent()
    private var api = ApiClient.instance()


    fun fecthIndonesiaData() {
        state.value = IndonesiaState.isLoading(true)

        api.fetchIndonesiaData().enqueue(object : Callback<List<Indonesia>> {


            override fun onFailure(call: Call<List<Indonesia>>, t: Throwable) {
                state.value = IndonesiaState.isLoading(false)
                state.value = IndonesiaState.isError(t.message)

                Log.d(TAG, "Response ${t.message}")

            }

            override fun onResponse(call: Call<List<Indonesia>>, response: Response<List<Indonesia>>) {
                Log.d(TAG, "Response $response")

                state.value = IndonesiaState.isLoading(false)
                if (response.isSuccessful) {
                    state.value = IndonesiaState.isSuccess(response.message())

                    val r = response.body()
                    indonesias.postValue(r)

                }


            }

        })
    }

    fun getState()  = state
    fun getIndonesias() = indonesias


}

sealed class IndonesiaState {
    data class isLoading(var state: Boolean = false) : IndonesiaState()
    data class isSuccess(var msg: String) : IndonesiaState()
    data class isError(var err: String?) : IndonesiaState()


}