package com.use.um.umcovid19.utils

import android.content.Context

class Constants {

    companion object{
        const val API = "https://api.kawalcorona.com/"
        const val USERS_PATH = "Users"
        const val PROVINSI_PATH = "Provinsi"

        fun getToken(context : Context) : String{

            val pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE)
            val token = pref.getString("TOKEN", "undefined")

            return token!!
        }
        fun getID(context : Context) : Int{

            val pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE)
            val id = pref.getInt("ID", 0)

            return id!!
        }

        fun setToken(context : Context, token : String, id : Int){
            val pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE)
            pref.edit().apply {
                putString("TOKEN", token)
                putInt("ID", id)
                apply()
            }
        }
        fun clearToken(context : Context){
            val pref = context.getSharedPreferences("USER", Context.MODE_PRIVATE)
            pref.edit().clear().apply()
        }

        fun isValidEmail (email : String) = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        fun isValidPassword (passwrod : String) = passwrod.length > 8

    }


}