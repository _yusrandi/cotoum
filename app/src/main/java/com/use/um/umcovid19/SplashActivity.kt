package com.use.um.umcovid19

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        doWork()
    }

    private fun doWork() {
        Handler().postDelayed({ startApp() }, 5 * 100.toLong())
    }
    private  fun startApp(){
        startActivity(Intent(this, HomeActivity::class.java ).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }).also {
            finish()
        }
    }
}
