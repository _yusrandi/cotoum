package com.use.um.umcovid19

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.use.um.umcovid19.models.Users
import com.use.um.umcovid19.utils.Constants
import com.use.um.umcovid19.utils.UserLocationListener
import com.use.um.umcovid19.viewmodels.UsersViewModel
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.toolbar.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, BottomNavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val TAG = "MapsActivity"
        const val DEFAULT_ZOOM = 16f

    }

    private lateinit var mapView: MapView
    private lateinit var mMap: GoogleMap
    private lateinit var mAuth: FirebaseAuth
    private lateinit var usersViewModel: UsersViewModel
    private var userList = listOf<Users>()
    private lateinit var mUserRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        mAuth = FirebaseAuth.getInstance()
        usersViewModel = UsersViewModel()
        usersViewModel.init()

        mUserRef = FirebaseDatabase.getInstance().reference.child(Constants.USERS_PATH)

        toolbar_title.text = "Monitoring User"
        maps_navigation.selectedItemId = R.id.maps
        maps_navigation.setOnNavigationItemSelectedListener(this)

        mapView = findViewById<MapView>(R.id.maps_mapview)
        if (mapView != null) {
            mapView.onCreate(null)
            mapView.onResume()
            mapView.getMapAsync(this)
        }
        usersViewModel.getUsers().observe(this, Observer {
            Log.d(TAG, "$it")
            userList = it
            it.forEach {
                val loc = it.location.split(",")
                mMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            java.lang.Double.valueOf(
                                loc[0]
                            ), java.lang.Double.valueOf(loc[1])
                        )
                    ).title(it.phone).snippet(it.time)
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.my_location))
                )
            }


        })
        initCurrentUserData()
    }

    private fun initCurrentUserData() {
        mUserRef.child(mAuth.uid.toString()).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val location = snapshot.child("location").value.toString()

                val loc = location.split(",")
                val latlng =  LatLng(
                    java.lang.Double.valueOf(
                        loc[0]
                    ), java.lang.Double.valueOf(loc[1])
                )
                moveCamera(latlng, DEFAULT_ZOOM)

            }

        })
    }

    override fun onStart() {
        super.onStart()
        if (mAuth != null) UserLocationListener(this, mAuth.uid.toString())
        usersViewModel.fecthUserData(this)
    }

    override fun onMapReady(p0: GoogleMap) {
        mMap = p0
        Log.d(TAG, "onMapReady")
    }

    private fun moveCamera(latLng: LatLng, zoom: Float) {
        Log.d(
            TAG,
            "moveCamera  : moving camera to lat " + latLng.latitude + " and lng " + latLng.longitude
        )
        val latPos = latLng.latitude
        val longPos = latLng.longitude
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        mMap.addMarker(
            MarkerOptions().position(LatLng(latPos, longPos)).title("My Position")
                .snippet("here is your").icon(BitmapDescriptorFactory.fromResource(R.mipmap.my_location))
        )
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.maps -> {
                true
            }
            R.id.tips -> {
                startActivity(Intent(applicationContext, TipsActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.info -> {
                startActivity(Intent(applicationContext, InfoActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.main -> {
                startActivity(Intent(applicationContext, HomeActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            else -> false
        }
    }
}
