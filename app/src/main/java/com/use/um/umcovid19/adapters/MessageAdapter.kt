package com.use.um.umcovid19.adapters

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.use.um.umcovid19.R
import com.use.um.umcovid19.models.Message
import kotlinx.android.synthetic.main.message_single_layout_recipient.view.*
import kotlinx.android.synthetic.main.message_single_layout_sender.view.*

class MessageAdapter(private var messages: MutableList<Message>, private var context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val TAG = "MessageAdapter"
    }

    private var SENDER = 0
    private var RECIPIENT = 1
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mCurrentUserId: String
    private lateinit var mUserId: String

    private lateinit var onItemClickCallback: OnItemClickCallback
    private lateinit var onLongItemClickCallback: OnLongItemClickCallback

    private var key: String = ""
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    fun setOnLongItemClickCallback(onLongItemClickCallback: OnLongItemClickCallback) {
        this.onLongItemClickCallback = onLongItemClickCallback
    }

    fun setmessages(r: List<Message>) {
        messages.clear()
        messages.addAll(r)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        mAuth = FirebaseAuth.getInstance()
        val f = messages[position]

        return if (f.isReceived == false) {
            SENDER
        } else RECIPIENT

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val inflater = LayoutInflater.from(parent.context);

        return when (viewType) {
            SENDER -> {
                val view = inflater.inflate(R.layout.message_single_layout_sender, parent, false)
                return ViewHolderSender(view)
            }
            RECIPIENT -> {
                val view = inflater.inflate(R.layout.message_single_layout_recipient, parent, false)
                return ViewHolderRecipent(view)
            }
            else -> {
                val view = inflater.inflate(R.layout.message_single_layout_sender, parent, false)
                return ViewHolderSender(view)
            }
        }

    }

    private fun configureSenderView(viewHolderSender: ViewHolderSender, position: Int) {
        val senderFireMessage: Message = messages[position]
        viewHolderSender.getSenderMessageTextView().text = senderFireMessage.message

        viewHolderSender.itemView.setOnClickListener() {
            onItemClickCallback.onItemClick(senderFireMessage)
        }
    }

    private fun configureRecipentView(viewHolderSender: ViewHolderRecipent, position: Int) {
        val senderFireMessage: Message = messages[position]
        viewHolderSender.getRecipintMessageText().text = senderFireMessage.message

        viewHolderSender.itemView.setOnClickListener() {
            onItemClickCallback.onItemClick(senderFireMessage)
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        holder.itemView.setOnLongClickListener {
            onLongItemClickCallback.onLongItemClick(messages[holder.adapterPosition])
            true
        }


        when (holder.itemViewType) {
            SENDER -> configureSenderView(holder as ViewHolderSender, position)
            RECIPIENT -> configureRecipentView(holder as ViewHolderRecipent, position)
        }
    }


    class ViewHolderSender(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var mUsersDatabase: DatabaseReference
        private var id: String = ""

        fun bind(message: Message, context: Context, i: Int) {
            itemView.message_text_send.text = message.message

        }

        fun getSenderMessageTextView(): TextView {
            return itemView.message_text_send
        }

    }

    class ViewHolderRecipent(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var mUsersDatabase: DatabaseReference
        private var id: String = ""

        fun bind(message: Message, context: Context, i: Int) {
            itemView.message_text_rec.text = message.message

        }

        fun getRecipintMessageText(): TextView {
            return itemView.message_text_rec

        }

    }

    interface OnItemClickCallback {
        fun onItemClick(data: Message)
    }

    interface OnLongItemClickCallback {
        fun onLongItemClick(data: Message)
    }

}

