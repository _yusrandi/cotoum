package com.use.um.umcovid19.chatbot.helpers

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class CoroutinesAsyncTask<Params, Progress, Result> {
    open fun onPreExecute() {}
    abstract fun doInBackground(vararg params: Params?): Result?
    open fun onProgressUpdate(vararg values: Progress?) {}
    open fun onPostExecute(result: Result?) {}
    open fun onCancelled(result: Result?) {}

    protected fun publishProgress(vararg progress: Progress) {
        GlobalScope.launch(Dispatchers.Default) {
            onProgressUpdate(*progress)
        }
    }

    fun execute(vararg params: Params?) {
        GlobalScope.launch(Dispatchers.Default) {
            val result = doInBackground(*params)
            withContext(Dispatchers.Default){
                onPostExecute(result)
            }
        }
    }

    fun cancel(matInterruptIfRunning: Boolean) {}
}