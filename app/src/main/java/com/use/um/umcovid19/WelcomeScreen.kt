package com.use.um.umcovid19

import android.annotation.SuppressLint
import com.stephentuso.welcome.BasicPage
import com.stephentuso.welcome.WelcomeActivity
import com.stephentuso.welcome.WelcomeConfiguration

@SuppressLint("Registered")
class WelcomeScreen: WelcomeActivity(){
    override fun configuration(): WelcomeConfiguration {
        return WelcomeConfiguration.Builder(this) //                .defaultTitleTypefacePath("Montserrat-Bold.ttf")
            //                .defaultHeaderTypefacePath("Montserrat-Bold.ttf")
            .page(
                BasicPage(
                    R.drawable.logo_aplikasi,
                    "Membantu Pelacakan untuk Menghentikan Penyebaran COVID - 19",
                    "Dengan mengaktifkan akses bluetooth, Anda akan membantu instansipemerintahanterkait dalam melakukan pelacakan untuk menghentikanpenyebaran Corona virus Disease (COVID - 19)."
                )
                    .background(R.color.orange_background)
            )
            .page(
                BasicPage(
                    R.drawable.logo_aplikasi,
                    "Notifikasi zona merah dan keramaian",
                    "Dengan mengaktifkan akses lokasi,  Anda akan mendapatkannotifikasi lansung  dari peduliLindungi jika Anda memasuki danberada lebih dari 30 menit di zona merah, yaitu area atau kelurahanyang sudah terdata bahwa ada orang yang terinfeksi COVID - 19 positifatau ada Pasien Dalam Pengawasan Kami juga akan memberikannotif jika Anda terdeteksi berada di keramaian dalam waktu yang cukup lama."
                )
                    .background(R.color.red_background)
            )
            .page(
                BasicPage(
                    R.drawable.logo_aplikasi,
                    "Pemeriksaan Kesehatan",
                    "Jika Anda memerlukan bantuan tenaga kesehatan, Anda bisamenggunakan fitur Teledokter untuk melakukan pemeriksaankesehatan mandiri an berkonsultasi dengan tenaga kesehatan terkaitkondisi kesehatan Anda."
                )
                    .background(R.color.blue_background)
            )

            .swipeToDismiss(true)
            .exitAnimation(android.R.anim.fade_out)
            .build()
    }

    fun welcomeKey(): String? {
        return "WelcomeScreen"
    }


}