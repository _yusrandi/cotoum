package com.use.um.umcovid19.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.use.um.umcovid19.R
import com.use.um.umcovid19.models.Kecamatan
import kotlinx.android.synthetic.main.item_main.view.*

class MainAdapter(private val context: Context): RecyclerView.Adapter<MainAdapter.MainViewlHolder>() {

    private var dataList = mutableListOf<Kecamatan>()

    fun setList(data: MutableList<Kecamatan>){
        dataList.clear()
        dataList = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewlHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_main, parent, false)

        return MainViewlHolder(view)
    }

    override fun onBindViewHolder(holder: MainViewlHolder, position: Int) {
        holder.bindView(dataList[position])
    }

    override fun getItemCount(): Int {
        return if(dataList.size>0) dataList.size else 0
    }

    inner class MainViewlHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bindView(kecamatan: Kecamatan){
//            Glide.with(context).load(users.img).into(itemView.item_main_img)
            itemView.item_main_title.text = "Kecamatan ${kecamatan.name}"
            itemView.item_main_positif.text = "${kecamatan.positif} Jiwa"
            itemView.item_main_sembuh.text = "${kecamatan.sembuh} Jiwa"
            itemView.item_main_meninggal.text = "${kecamatan.meninggal} Jiwa"
        }
    }
}