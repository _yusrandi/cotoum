package com.use.um.umcovid19

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.stephentuso.welcome.WelcomeHelper
import com.use.um.umcovid19.adapters.MainAdapter
import com.use.um.umcovid19.models.Kecamatan
import com.use.um.umcovid19.utils.Constants
import com.use.um.umcovid19.viewmodels.CaseState
import com.use.um.umcovid19.viewmodels.CaseViewModel
import kotlinx.android.synthetic.main.activity_home.*
import java.io.IOException
import java.util.*


class HomeActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    GoogleApiClient.ConnectionCallbacks,
LocationListener,
GoogleApiClient.OnConnectionFailedListener{
    companion object{
        const val REQUEST_ENABLE_BLUETOOTH = 11
        const val TAG = "HomeActivity"
    }
    private lateinit var adapter: MainAdapter

    private lateinit var bluetoothAdapter: BluetoothAdapter

    private lateinit var loadingDialog: SweetAlertDialog
    private lateinit var errorLoading: SweetAlertDialog
    private lateinit var successLoading: SweetAlertDialog

    private lateinit var welcomeScreen: WelcomeHelper

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mUserRef: DatabaseReference

    private var permissionCode = 1
    private var permissions = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN

    )

    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private val UPDATE_INTERVAL: Long = 0
    private val FASTEST_UPDATE_INTERVAL = 1000 * 120 * 1.toLong()
    private val MAX_WAIT_TIME = UPDATE_INTERVAL * 5

    private val viewModel by lazy { ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(CaseViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(home_toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        home_toolbar.title = title

        mAuth = FirebaseAuth.getInstance()
        mUserRef = FirebaseDatabase.getInstance().reference.child(Constants.USERS_PATH)

        if (!hasPermissions(this, permissions)) { ActivityCompat.requestPermissions(this, permissions, permissionCode) }

        welcomeScreen = WelcomeHelper(this, WelcomeScreen::class.java)
        welcomeScreen.show(savedInstanceState)

        home_navigation.selectedItemId = R.id.main
        home_navigation.setOnNavigationItemSelectedListener(this)

        viewModel.init()

        buildGoogleApiClient()

        initUserData()
        initRecyclerView()
        layout_setting.setOnClickListener {
            startActivity(Intent(this, SettingActivity::class.java).apply {
                putExtra("loc", home_tv_location.text.toString())
            })
        }
        layout_bluetooth.setOnClickListener {
            if(bluetoothAdapter.isEnabled) {
                SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Bluetooth") //
                    .setContentText("Turn Off Bluetooth ?")
                    .setConfirmText("Yes, sure")
                    .setConfirmClickListener { sDialog ->
                        alertWarning("Bluetooth dimatikan", 1)
                        sDialog.dismissWithAnimation()
                    }
                    .setCancelButton("No") { ssDialog -> ssDialog.dismissWithAnimation() }
                    .show()

            } else {
                alertSuccess("Bluetooth dihidupkan", 1)

            }
        }
        viewModel.getCases().observe(this, androidx.lifecycle.Observer {
            Log.d(TAG, "getCases $it")
            adapter = MainAdapter(this)
            home_rv.layoutManager = LinearLayoutManager(this)
            home_rv.adapter = adapter
               adapter.setList(it as MutableList<Kecamatan>)

        })
        viewModel.getState().observe(this, androidx.lifecycle.Observer {
            Log.d(TAG, "getState $it")
            handleUIState(it)
        })

        fab_home.setOnClickListener {
            startActivity(Intent(this@HomeActivity, ChatBotActivity::class.java))
        }
    }

    private fun handleUIState(it: CaseState) {
        when (it) {

            is CaseState.IsError -> {
                isLoading(false)
                showToast(it.err.toString())
            }
            is CaseState.ShowToast -> showToast(it.message)

            is CaseState.IsSuccess -> {
                isLoading(false)
            }
            is CaseState.IsLoading -> isLoading(it.state)
            else -> showToast("Undefined")
        }
    }

    private fun isLoading(state: Boolean) {

        if (state) {
            home_loading.visibility = View.VISIBLE
        } else {
            home_loading.visibility = View.GONE
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "On onResume")

    }
    private fun initUserData() {
        mUserRef.child(mAuth.uid.toString()).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(error: DatabaseError) {
            }
            override fun onDataChange(snapshot: DataSnapshot) {
                val location = snapshot.child("location").value.toString()
                val suhu = snapshot.child("suhu").value.toString()
                home_tv_suhu.text = "$suhu'"

            }
        })
    }
    private fun initRecyclerView(){


    }


    private fun hasPermissions(context: Context, permissions: Array<String>): Boolean {

        if (context != null && permissions != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }
    override fun onStart() {
        super.onStart()

        if(mAuth.currentUser == null) sendToStart()

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        checkBluetoothState()
        registerReceiver(devicesFoundReciever, IntentFilter(BluetoothDevice.ACTION_FOUND))
        registerReceiver(devicesFoundReciever, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
        registerReceiver(devicesFoundReciever, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))

        Log.d(TAG, "On onStart")


    }
    private fun checkBluetoothState(){
        if(bluetoothAdapter == null){
            showToast("Bluetooth is not supported on your device")
        }else{
            if(bluetoothAdapter.isEnabled){
                img_bluetooth.setImageResource(R.drawable.ic_bluetooth_on)
                if(bluetoothAdapter.isDiscovering)
                    showToast("Device Discovering Proses . . .")
                else {
                    showToast("Bluetooth is Enabled")
                    bluetoothAdapter.startDiscovery()
                }
            }else{
                img_bluetooth.setImageResource(R.drawable.ic_bluetooth_off)
                showToast("You need to enable Bluetooth")
                val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH)
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_ENABLE_BLUETOOTH) checkBluetoothState() else showToast("Bluetooth must be turn on")
    }
    private val devicesFoundReciever = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent!!.action

            if(BluetoothDevice.ACTION_FOUND == action){
                home_card_scanning.setCardBackgroundColor(Color.RED)
                home_tv_scanning.setTextColor(Color.WHITE)
                home_tv_scanning.text = "Mohon Jaga Jarak"
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
            }else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                home_tv_scanning.text = "Terima Kasih Telah Menjaga Jarak Aman"
                home_tv_scanning.setTextColor(Color.BLACK)
                home_card_scanning.setCardBackgroundColor(Color.WHITE)
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED == action) {
                home_tv_scanning.text = "Scanning in Progress . . ."
                home_tv_scanning.setTextColor(Color.BLACK)
                home_card_scanning.setCardBackgroundColor(Color.WHITE)
            }
        }

    }
    private fun showToast(msg: String) = Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.main -> {
                true
            }
            R.id.tips -> {
                startActivity(Intent(applicationContext, TipsActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.info -> {
                startActivity(Intent(applicationContext, InfoActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            R.id.maps -> {
                startActivity(Intent(applicationContext, MapsActivity::class.java)).also { finish() }
                overridePendingTransition(0, 0)
                true
            }
            else -> false
        }
    }
    private fun sendToStart() {
        startActivity(Intent(this, SignUpActivity::class.java))
        finish()
    }
    private fun alertError(msg: String, what: Int) {
        errorLoading = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
        errorLoading.titleText = "Terjadi Kesalahan !"
        errorLoading.contentText = msg
        errorLoading.setCancelable(false)
        errorLoading.setConfirmClickListener { errorLoading.dismissWithAnimation() }
        errorLoading.show()


    }
    private fun alertWarning(msg: String, what: Int) {
        errorLoading = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        errorLoading.titleText = "Warning !"
        errorLoading.contentText = msg
        errorLoading.setCancelable(false)
        errorLoading.setConfirmClickListener { errorLoading.dismissWithAnimation() }
        errorLoading.show()

        if(what == 1){
            bluetoothAdapter.disable()
            img_bluetooth.setImageResource(R.drawable.ic_bluetooth_off)
            home_card_scanning.setCardBackgroundColor(Color.RED)
            home_tv_scanning.setTextColor(Color.WHITE)
            home_tv_scanning.text = "Please Enable Your Bluetooth"
        }

    }
    private fun alertSuccess(msg: String, what: Int) {
        successLoading = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
        successLoading.titleText = "Berhasil"
        successLoading.contentText = msg

        successLoading.setConfirmClickListener(SweetAlertDialog.OnSweetClickListener {
            successLoading.dismissWithAnimation()

            if (what == 1) {
                img_bluetooth.setImageResource(R.drawable.ic_bluetooth_on)
                bluetoothAdapter.enable()
                checkBluetoothState()
            }

        })

        successLoading.show()

    }

    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .enableAutoManage(this, this)
            .addApi(LocationServices.API)
            .build()
        createLocationRequest()
    }
    private fun createLocationRequest() {
        Log.d(TAG, "createLocationRequest")
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = UPDATE_INTERVAL

        mLocationRequest.fastestInterval = FASTEST_UPDATE_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mLocationRequest.maxWaitTime = MAX_WAIT_TIME
    }

    protected fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        val pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient, mLocationRequest, this
        )
        Log.d(TAG, "Location update started ..............: ")
    }
    fun getUserAdress(p0: Location): String{
        var userAdres: String = ""
        val geocoder = Geocoder(this, Locale.getDefault())
        val addresses: List<Address>

        try {
            addresses = geocoder.getFromLocation(p0.latitude, p0.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                val city: String = addresses[0].locality
                val state: String = addresses[0].adminArea
                val sub_admin: String = addresses[0].subAdminArea
                val country: String = addresses[0].countryName
                val postalCode: String = addresses[0].postalCode
                val knownName: String = addresses[0].featureName
                Log.d(TAG, addresses[0].toString())
                userAdres = "$city, $sub_admin"
                Log.d(TAG, "Adress $address")
            }
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return userAdres
    }
    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }
    override fun onConnectionSuspended(p0: Int) {
        TODO("Not yet implemented")
    }
    override fun onLocationChanged(location: Location) {
        Log.d(TAG, "onLocationChanged LatLang ${location.latitude}, ${location.longitude}")
        mUserRef.child(mAuth.uid.toString()).child("location").setValue("${location.latitude},${location.longitude}").addOnCompleteListener {
            if(it.isComplete) Log.d(TAG, "Succes update location") else Log.d(TAG, "UnSucces update location")
        }

        val geocoder = Geocoder(this, Locale.getDefault())
        val addresses: List<Address>

        try {
            addresses = geocoder.getFromLocation(location.latitude, location.longitude, 1)
            if (addresses != null && addresses.isNotEmpty()) {
                val address: String = addresses[0].getAddressLine(0)
                val city: String = addresses[0].locality
                val state: String = addresses[0].adminArea
                val sub_admin: String = addresses[0].subAdminArea
                val country: String = addresses[0].countryName
                val postalCode: String = addresses[0].postalCode
                val knownName: String = addresses[0].featureName
                Log.d(TAG, addresses[0].toString())
                home_tv_location.text = "$city, $sub_admin"
                Log.d(TAG, "Provinsi $state, Kabupaten $sub_admin")

                viewModel.fecthCaseData(this, state, sub_admin)

            }
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            Log.e(TAG, "${e.message}")
        }
    }
    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("Not yet implemented")
    }


}

